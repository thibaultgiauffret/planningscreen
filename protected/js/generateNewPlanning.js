/**
 * Generate an empty planning table
 */
function showNewPlanningModal() {

    // Load the teachers table
    let newPlanningTeachersTableRows = document.getElementById("newPlanningTeachersTableRows");
    // Empty the table
    newPlanningTeachersTableRows.innerHTML = "";

    // For each teacher, create a row
    for (let i = 0; i < teachers.length; i++) {
        let teacher = teachers[i];
        let row = document.createElement("tr");
        row.id = "newPlanningTeacherRow" + teacher.id;

        let IDCell = document.createElement("td");
        IDCell.innerHTML = teacher.id;

        row.appendChild(IDCell);

        let cell = document.createElement("td");
        cell.innerHTML = "<input type='text' class='form-control' value='" + teacher.name + "'>";
        row.appendChild(cell);

        // Add actions (validate, delete)
        let actionCell = document.createElement("td");

        let deleteButton = document.createElement("button");
        deleteButton.classList.add("btn");
        deleteButton.classList.add("btn-danger");
        deleteButton.innerHTML = "<i class='fas fa-trash'></i>";
        deleteButton.onclick = function () {
            // Delete the row
            if (confirm("Voulez-vous vraiment supprimer " + teacher.name + " du planning ?")) {
                let row = document.getElementById("newPlanningTeacherRow" + teacher.id);
                row.remove();
            }
        }

        actionCell.appendChild(deleteButton);

        row.appendChild(actionCell);

        newPlanningTeachersTableRows.appendChild(row);
    }

    let newPlanningModal = document.getElementById("newPlanningModal");
    let modalInstance = new bootstrap.Modal(newPlanningModal);
    modalInstance.show();
}

function addTeacherToNewPlanning() {
    let newPlanningTeachersTableRows = document.getElementById("newPlanningTeachersTableRows");

    // Get the last teacher ID
    let lastTeacherID = teachers[teachers.length - 1].id;

    let row = document.createElement("tr");
    row.id = "newPlanningTeacherRow" + (lastTeacherID + 1);

    let IDCell = document.createElement("td");
    IDCell.innerHTML = lastTeacherID + 1;

    row.appendChild(IDCell);

    let cell = document.createElement("td");
    cell.innerHTML = "<input type='text' class='form-control' id='newPlanningTeacherRow" + (lastTeacherID + 1) + "_text' value=''>";
    row.appendChild(cell);

    // Add actions (validate, delete)
    let actionCell = document.createElement("td");

    let deleteButton = document.createElement("button");
    deleteButton.classList.add("btn");
    deleteButton.classList.add("btn-danger");
    deleteButton.innerHTML = "<i class='fas fa-trash'></i>";
    deleteButton.onclick = function () {
        // Delete the row
        let name = document.getElementById("newPlanningTeacherRow" + (lastTeacherID + 1) + "_text").value;
        if (confirm("Voulez-vous vraiment supprimer " + name + " du planning ?")) {

            let row = document.getElementById("newPlanningTeacherRow" + (lastTeacherID + 1));
            row.remove();
        }
    }

    actionCell.appendChild(deleteButton);

    row.appendChild(actionCell);

    newPlanningTeachersTableRows.appendChild(row);
}

function generateEmptyPlanning() {
    socket.emit('generateEmptyPlanning');
}
