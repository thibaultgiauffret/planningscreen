// Variables are declared in admin.js
let planning;

function changeSelectedDay(day) {
    selectedDay = day;
    getDataEdit(event.target.value)
}

function getDataEdit(day) {

    // Set the selected date
    selectedDate = day;
    console.log("getData is called with value : " + selectedDate);

    // Clear the rows with classeName = "tableRow"
    rows = document.getElementsByClassName("tableRow");
    while (rows.length > 0) {
        rows[0].parentNode.removeChild(rows[0]);
    }

    planning = database.planning[selectedDate];

    buildRowsEdit();
    addEditOverlayToCell();

}

function buildRowsEdit() {
    let planningTable = document.getElementById('planningTable');
    let rows = planning.rows;
    // For each room in rows, add a cell
    for (let i = 0; i < rows.length; i++) {
        // The first cell is the name of the teacher
        let row = planningTable.insertRow(-1);
        // Set class name to the row
        row.className = "tableRow";
        // Add a th cell for the teacher name
        let cell = row.insertCell(-1);
        teacherId = rows[i].id;
        // Find the teacher in teachers with the teacherId
        let teacher = teachers.find(t => t.id === teacherId);
        cell.innerHTML = teacher.name;
        cell.className = "name";
        // Create a counter for the number of cells to merge
        let firstCell;
        // For each room in rows[i].rooms, add a cell
        for (let j = 0; j < rows[i].rooms.length; j++) {
            if (rows[i].rooms[j] == "") {
                cell = row.insertCell(-1);
                // Add the class "cell" to the cell
                cell.className = "emptyCell";
                // Add data attribute to the cell with j value
                cell.setAttribute("data-timeslot", j);
                cell.setAttribute("data-teacher", teacherId);
                continue;
            }

            cell = row.insertCell(-1);
            cell.colSpan = 1;
            firstCell = cell;
            // Add the class "cell" to the cell
            cell.className = "cell";
            // Add data attribute to the cell with j value
            cell.setAttribute("data-timeslot", j);
            cell.setAttribute("data-teacher", teacherId);
            // Check the room type
            let roomType = rooms.rooms[rows[i].rooms[j]].type;
            let color = rooms.roomColors[roomType];
            let icon = rooms.roomIcons[roomType];
            if (color == undefined) {
                color = "warning";
                icon = "";
            }
            // Create a div with alert class
            let div = document.createElement('div');
            div.className = "alert bg-" + color + " inline-block mb-0 fw-bolder";
            div.style = "position: relative;";
            if (icon != "") {
                // Add the icon to the background
                div.innerHTML += `
                        <div class="backgroundIcon alert-` + color + `-emphasis"> 
                            <div class="icon">
                                <i class="fas fa-` + icon + `"></i>
                        </div> `
            }
            div.innerHTML += rows[i].rooms[j];
            cell.innerHTML = div.outerHTML;

        }
    }
}

function addEditOverlayToCell() {

    console.log("addEditOverlayToCell is called");

    let cells = document.getElementsByClassName("cell");

    for (let i = 0; i < cells.length; i++) {

        let cell = cells[i];
        cell.style = "position: relative;";

        let editDiv = document.createElement("div");
        // Show the buttons on hover
        editDiv.style = "display: none;";

        // Show the buttons on hover
        cell.onmouseover = function () {
            // Show the buttons at the center of the cell
            editDiv.style = "display: flex; flex-direction: row; justify-content: center; align-items: center; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);";
        };

        // Hide the buttons on mouseout
        cell.onmouseout = function () {
            editDiv.style = "display: none;";
        };

        // Find each cell in the table and add two buttons : edit and delete
        let editButton = document.createElement("button");
        editButton.innerHTML = "<i class='fas fa-edit'></i>";
        editButton.className = "btn btn-sm btn-secondary";
        editButton.onclick = function () {
            editCell(cell);
        };

        let deleteButton = document.createElement("button");
        deleteButton.innerHTML = "<i class='fas fa-trash-alt'></i>";
        deleteButton.className = "btn btn-sm btn-danger";
        deleteButton.onclick = function () {
            deleteCell(cell);
        };

        editDiv.appendChild(editButton);
        editDiv.appendChild(deleteButton);

        cell.appendChild(editDiv);
    }

    let emptyCells = document.getElementsByClassName("emptyCell");

    for (let i = 0; i < emptyCells.length; i++) {

        let cell = emptyCells[i];
        cell.style = "position: relative;";

        let editDiv = document.createElement("div");
        // Show the buttons on hover
        editDiv.style = "display: none;";

        // Show the buttons on hover
        cell.onmouseover = function () {
            // Show the buttons at the center of the cell
            editDiv.style = "display: flex; flex-direction: row; justify-content: center; align-items: center; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);";
        };

        // Hide the buttons on mouseout
        cell.onmouseout = function () {
            editDiv.style = "display: none;";
        };

        // Find each cell in the table and add two buttons : edit and delete
        let addButton = document.createElement("button");

        addButton.innerHTML = "<i class='fas fa-plus'></i>";
        addButton.className = "btn btn-sm btn-success";
        addButton.onclick = function () {
            showAddCell(cell);
        };

        editDiv.appendChild(addButton);
        cell.appendChild(editDiv);
    }
}

function showAddCell(cell) {
    console.log("showAddCell is called");

    // Get the addCellModalBody and add a dropdown with the rooms
    let addCellModalBody = document.getElementById("addCellModalBody");
    addCellModalBody.innerHTML = "";
    let dropdown = document.createElement("select");
    dropdown.className = "form-select";
    dropdown.id = "roomSelect";

    for (let key in rooms.rooms) {
        let option = document.createElement("option");
        option.value = key;
        option.innerHTML = key;
        dropdown.appendChild(option);
    }

    addCellModalBody.appendChild(dropdown);

    // Get the addCellModalButton and add a click event
    let addCellModalButton = document.getElementById("addCellModalButton");
    addCellModalButton.onclick = function () {
        // Get the selected room
        let room = document.getElementById("roomSelect").value;
        addCell(cell, room);
    };

    let modal = document.getElementById("addCellModal");
    let modalInstance = new bootstrap.Modal(modal);
    modalInstance.show();
}

function addCell(cell, room) {
    console.log("addCell is called");

    let teacherId = cell.getAttribute("data-teacher");
    let timeslot = cell.getAttribute("data-timeslot");

    // Find the current day in database.planning
    let planning = database.planning[selectedDate];

    // Find the teacher in planning.rows with the teacherId
    let teacher = planning.rows.find(t => t.id === parseInt(teacherId));

    // Add a room to the teacher.rooms
    teacher.rooms[timeslot] = room;

    // Add the new planning to the database
    database.planning[selectedDate] = planning;

    // Update the table with the new data
    getDataEdit(selectedDate);

    // Enable the apply button
    enableApplyBtn();
}

function editCell(cell) {
    // use showAddCell function to edit the cell
    showAddCell(cell);
}

function deleteCell(cell) {
    console.log("deleteCell is called");

    let teacherId = cell.getAttribute("data-teacher");
    let timeslot = cell.getAttribute("data-timeslot");

    // Find the current day in database.planning
    let planning = database.planning[selectedDate];

    console.log("planning : " + JSON.stringify(planning));

    // Find the teacher in planning.rows with the teacherId
    let teacher = planning.rows.find(t => t.id === parseInt(teacherId));

    // Add a room to the teacher.rooms
    teacher.rooms[timeslot] = "";

    // Add the new planning to the database
    database.planning[selectedDate] = planning;

    // Update the table with the new data
    getDataEdit(selectedDate);

    // Enable the apply button
    enableApplyBtn();
}