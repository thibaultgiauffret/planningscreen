
let database;
let rooms;
let teachers;
let messages;
let isTempMoreRecent = false;

let selectedDay = currentDate();
let selectedWeek = currentWeek();

var timer;

var socket = io();

function init() {

    document.addEventListener('DOMContentLoaded', function () {
        document.getElementById('date').value = selectedDay;
    });

    socket.on('connect', function () {
        console.log('Connected to the server');
        socket.emit('getDatabase');
        socket.emit('getHistory');
    });

    // On refresh, the page will be reloaded (disabled in admin temporarily)
    // socket.on('refresh', function () {
    //     location.reload();
    // });

    // Get the data from the server
    socket.on('getDatabase', function (data) {
        console.log("Database received");
        database = data[0];
        rooms = data[1];
        teachers = data[2];
        messages = data[3];
        isTempMoreRecent = data[4];
        getDataEdit(selectedDay);

        if (isTempMoreRecent) {
            let loadTempButton = document.createElement("button");
            loadTempButton.classList.add("btn");
            loadTempButton.classList.add("btn-warning");
            loadTempButton.innerHTML = "Charger la base de données temporaire";
            loadTempButton.onclick = function () {
                socket.emit('getTempDatabase');
            }
            displayMessage("Des modifications temporaires sont disponibles dans l'historique.", "warning", loadTempButton);
        }
        // Disable the apply button
        disableApplyBtn();

    });

    socket.on('getTempDatabase', function (data) {
        console.log("Temp database received");
        database = data;
        getDataEdit(selectedDay);
        // Enable the apply button
        enableApplyBtn();
    });

    // Get error message from the server
    socket.on('errorMessage', function (message) {
        displayMessage(message, "danger");
    });

    // Get the history list from the server
    socket.on('getHistory', function (history) {
        console.log("History received");
        if (history.length == 0) {
            return;
        }
        let historyList = document.getElementById("historyList");
        historyList.innerHTML = "";

        // Add the actual database to the history list
        let listItem = document.createElement("li");
        listItem.classList.add("dropdown-item");
        listItem.innerHTML = "Actuelle";
        listItem.onclick = function () {
            socket.emit('getDatabase');
        }
        historyList.appendChild(listItem);

        // Add the temp database to the history list
        if (isTempMoreRecent) {
            let listItem = document.createElement("li");
            listItem.classList.add("dropdown-item");
            listItem.innerHTML = "Temporaire (plus récente)";
            listItem.onclick = function () {
                socket.emit('getTempDatabase');
            }
            historyList.appendChild(listItem);
        }

        // Add a separator
        let separator = document.createElement("li");
        separator.classList.add("dropdown-divider");
        historyList.appendChild(separator);

        history.reverse().forEach(file => {
            let date = new Date(file.date);
            let listItem = document.createElement("li");
            listItem.classList.add("dropdown-item");
            listItem.innerHTML = date.toLocaleString();
            listItem.onclick = function () {
                socket.emit('loadHistory', file.name);
            }
            historyList.appendChild(listItem);
        });
    });

    // Load the history file from the server
    socket.on('loadHistory', function (data) {
        console.log("History loaded");
        database = data;
        getDataEdit(selectedDay);
        displayMessage("L'ancienne version a bien été chargée !", "success");
        // Enable the apply button
        enableApplyBtn();
        // Disable the autosave timer
        killTimer();
    });
}

/**
 * Functions to handle auto save
 */
function killTimer() {
    console.log("Autosave timer stopped");
    clearInterval(timer);
}

function startTimer() {
    console.log("Autosave timer started");
    if (timer === undefined) {
        timer = setInterval(() => {
            sendDatabase(1);
            socket.emit('getHistory');
        }, 60000);
    }
}

/**
 * Function to logout
 */
function logout() {
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    window.location.href = "/auth?message=logout";
}

/**
 * Function to send the modified database to the server
 */

function sendDatabase(temp = 0) {
    // Timestamp the database
    database.lastModified = Date.now();
    socket.emit('updateDatabase', temp, database);
    if (temp == 0) {
        disableApplyBtn();
        displayMessage("Base de données sauvegardée !", "success");
    } else {
        displayMessage("Base de données sauvegardée automatiquement.", "success");
    }
}

/**
 * Function to display a message
 */

function displayMessage(message, type, button = null) {
    let notif = document.getElementById("notif");
    let notifTitle = document.getElementById("notifTitle");
    let notifHeader = document.getElementById("notifHeader");
    let notifMessage = document.getElementById("notifMessage");
    notifTitle.classList.remove("text-black");
    notifHeader.classList.remove("bg-danger");
    notifHeader.classList.remove("bg-warning");
    notifHeader.classList.remove("bg-success");

    if (type == "danger") {
        notifTitle.innerHTML = "Erreur";
    } else if (type == "warning") {
        notifTitle.innerHTML = "Attention";
        notifTitle.classList.add("text-black");
    } else if (type == "success") {
        notifTitle.innerHTML = "Succès";
    }
    notifHeader.classList.add("bg-" + type);
    notifMessage.innerHTML = message;

    // Add a button to the toast
    if (button != null) {
        let notifButton = document.getElementById("notifButton");
        notifButton.innerHTML = "";
        notifButton.appendChild(button);
        notifButton.classList.remove("d-none");
    } else {
        let notifButton = document.getElementById("notifButton");
        notifButton.innerHTML = "";
        notifButton.classList.add("d-none");
    }

    // Show the bootstrap toast
    let toast = new bootstrap.Toast(notif);
    toast.show();
}

function enableApplyBtn() {
    // Start the autosave timer
    startTimer();
    // Enable the apply button
    let applyButton = document.getElementById("btnSave");
    applyButton.disabled = false;
    // Change color
    applyButton.classList.remove("btn-secondary");
    applyButton.classList.remove("disabled");
    applyButton.classList.add("btn-success");
    applyButton.classList.add("flashing-button");
}

function disableApplyBtn() {
    // Stop the autosave timer
    killTimer();
    // Disable the apply button
    let applyButton = document.getElementById("btnSave");
    applyButton.disabled = true;
    // Change color
    applyButton.classList.remove("btn-success");
    applyButton.classList.remove("flashing-button");
    applyButton.classList.add("btn-secondary");
    applyButton.classList.add("disabled");
}