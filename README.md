# PlanningScreen

**Important note**: This project is still in early development and is not yet ready for production use.

PlanningScreen is a web app that allows you to display the day's schedule for each teacher.

## Installation

1. Clone the repository
2. Run `npm install` to install the dependencies
3. Run `npm start` to start the development server. The app will be available at `http://localhost:3000`