
var socket = io();
socket.on('connect', function () {
    console.log('Connected to the server');
});

// On page load, get the message from the url
var url_string = window.location.href;
var url = new URL(url_string);
var message = url.searchParams.get("message");
if (message == 'bad_credentials') {
    showMessage('Les identifiants sont incorrects&nbsp;!', 'error');
} else if (message == 'logout') {
    showMessage('Vous avez été déconnecté&nbsp;!', 'success');
} else if (message == 'need_token') {
    showMessage('Veuillez vous connecter pour accéder à cette page&nbsp;! ', 'error');
}

/*
    * Function to login
    * Send the credentials to the server
    * Get the jwt token
    * Save the token in a cookie
    * Redirect to the admin page
*/
function login() {
    var username = $("#username").val();
    var password = $("#password").val();
    console.log(username);
    console.log(password);
    // Send credentials to the server with socket.io
    socket.emit('login', { username: username, password: password });
    socket.on('login', (data) => {
        console.log(data);
        if (data.message == 'bad_credentials') {
            window.location.href = "/auth?message=bad_credentials";
            return;
        }
        // Get the jwt token
        var token = data.access_token;
        // Save the token in a cookie for 2 hour (not ideal, but enough for this project ?)
        document.cookie = "token=" + token + ";max-age=7200;path=/";
        // Redirect to the admin page
        window.location.href = "/admin";
    });
}

/*
    * Functions to login when the user press the enter key
*/
 $("#password").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#login").click();
    }
});
$("#username").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#login").click();
    }
});

/*
    * Function to show the messages
*/
function showMessage(message, type) {
    var div = document.getElementById('message');
    var icon;
    if (type == 'error') {
        icon = '<i class="fas fa-exclamation-triangle"></i>';
    } else {
        icon = '<i class="fas fa-check"></i>';
    }
    div.innerHTML = icon + "&nbsp;&nbsp;" + message;
    div.style.display = 'block';
    if (type == 'error') {
        div.className = 'alert alert-danger';
    } else {
        div.className = 'alert alert-success';
    }
}