let database;
let rooms;
let selectedDay = currentDate();
let selectedWeek = currentWeek();

/**
 * Initialize the page
 * @return {void}
 * @description Initialize the page by getting the data from the data.json
 * and loading the rows of the table
 **/
function init() {
    document.addEventListener('DOMContentLoaded', function () {
        // Get the data from the data.json
        getData(selectedDay);
        // Modify #firstDay and #lastDay to the current week
        document.getElementById('firstDay').innerHTML = selectedWeek[0];
        document.getElementById('lastDay').innerHTML = selectedWeek[1];
    });

    var socket = io();
    socket.on('connect', function () {
        console.log('Connected to the server');
        socket.emit('getDatabase');
    });
    // On refresh, the page will be reloaded
    socket.on('refresh', function () {
        location.reload();
    });

    // Get the data from the server
    socket.on('getDatabase', function (data) {
        console.log("Database received");
        database = data[0];
        rooms = data[1];
        getData(selectedDay);
    });

}

/*
    * Get the data from the data.json
    * @param {string} day The day to get the data from
    * @return {void}
    * @description Fetch the data from the data.json and load the rows
*/
function getData(day) {

    // Test if the day exists in the json
    if (!database.planning[day]) {
        console.log("No data for this day");
        displayMessage("Pas de données pour ce jour", "danger");
        return;
    }
    planning = database.planning[day];
    teachers = database.teachers;
    messages = database.messages;
    buildRows();
    buildMessages();

}

/*
    * Load the rows of the table
    * @return {void}
    * @description Load the rows of the table with the data from the data.json
    * The first cell is the name of the teacher
    * The other cells are the rooms of the teacher
    * If the room is empty, the cell is empty
    * If the room is not empty, the cell is a div with the room name
    * If the previous cell is empty, the cell is merged with the previous cell
*/
function buildRows() {
    let planningTable = document.getElementById('planningTable');
    let rows = planning.rows;
    // For each room in rows, add a cell
    for (let i = 0; i < rows.length; i++) {
        // The first cell is the name of the teacher
        let row = planningTable.insertRow(-1);
        // Set class name to the row
        row.className = "tableRow";
        // Add a th cell for the teacher name
        let cell = row.insertCell(-1);
        teacherId = rows[i].id;
        // Find the teacher in teachers with the teacherId
        let teacher = teachers.find(t => t.id === teacherId);
        cell.innerHTML = teacher.name;
        cell.className = "name";
        // Create a counter for the number of cells to merge
        let firstCell;
        // For each room in rows[i].rooms, add a cell
        for (let j = 0; j < rows[i].rooms.length; j++) {
            if (rows[i].rooms[j] == "") {
                cell = row.insertCell(-1);
                continue;
            }
            // If the previous cell is not empty, extend the previous cell and the room name is the same
            if (j > 0 && rows[i].rooms[j - 1] != "" && rows[i].rooms[j - 1] == rows[i].rooms[j]) {
                firstCell.colSpan += 1;
            } else {
                cell = row.insertCell(-1);
                cell.colSpan = 1;
                firstCell = cell;
                // Add the class "cell" to the cell
                cell.className = "cell";
                // Check the room type
                let roomType = rooms.rooms[rows[i].rooms[j]].type;
                let color = rooms.roomColors[roomType];
                let icon = rooms.roomIcons[roomType];
                if (color == undefined) {
                    color = "warning";
                    icon = "";
                }
                // Create a div with alert class
                let div = document.createElement('div');
                div.className = "alert bg-" + color + " inline-block mb-0 fw-bolder";
                div.style = "position: relative;";
                if (icon != "") {
                    // Add the icon to the background
                    div.innerHTML += `
                        <div class="backgroundIcon alert-` + color + `-emphasis"> 
                            <div class="icon">
                                <i class="fas fa-` + icon + `"></i>
                        </div> `
                }
                div.innerHTML += rows[i].rooms[j];
                cell.innerHTML = div.outerHTML;
            }
        }
    }
}

/*
    * Build messages
    * @return {void}
    * @description Build the messages in the message area
*/
function buildMessages() {
    // Empty the message area
    let messageArea = document.getElementById('messageArea');
    messageArea.innerHTML = "";
    // For each message in messages, add a div with "ticker__item" class
    for (let i = 0; i < messages.length; i++) {
        let div = document.createElement('div');
        div.className = "ticker__item";
        div.innerHTML = messages[i].message;
        messageArea.appendChild(div);
    }
}

/*
    * Display central message
    * @param {string} message The message to display
    * @param {string} type The type of message (default: primary)
    * @return {void}
    * @description Display a central message
*/
function displayMessage(message, type = "primary") {
    let centralMessage = document.getElementById('centralMessage');
    centralMessage.innerHTML = "<div class='alert alert-" + type + "'>" + message + "</div>";
    centralMessage.style.display = "block";
}