/*
    * Get the current date
    * @return {string} The current date at the YYYY-MM-DD format
    * @description Get the current date and return it at the YYYY-MM-DD format
*/
function currentDate() {
    let date = new Date();
    // Format month and day to have 2 digits
    let month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    let day = date.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    // Return the day of the week at the YYYY-MM-DD format
    return date.getFullYear() + "-" + month + "-" + day;
}

/*
    * Get the current week
    * @return {string} The current week at ("DD/MM/YYYY","DD/MM/YYYY") format
    * @description Get the current week and return it at the DD/MM/YYYY - DD/MM/YYYY format
    * The week starts on Monday
    * The week ends on Sunday
*/
function currentWeek() {
    let date = new Date();
    // Get the day of the week
    let day = date.getDay();
    // Get the date of the current day
    let currentDate = date.getDate();
    // Get the first day of the week
    let firstDay = currentDate - day + (day == 0 ? -6 : 1);
    // Get the last day of the week
    let lastDay = firstDay + 6;
    // Format the first and last day to have 2 digits
    if (firstDay < 10) {
        firstDay = "0" + firstDay;
    }
    if (lastDay < 10) {
        lastDay = "0" + lastDay;
    }
    // Format month to have 2 digits
    let month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    // Return the week at the ("DD/MM/YYYY","DD/MM/YYYY") format
    return [firstDay + "/" + month + "/" + date.getFullYear(), lastDay + "/" + month + "/" + date.getFullYear()];
}
