const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path');
var cookies = require("cookie-parser");
const { checkTokenMiddleware, SECRET } = require('./functions/checkTokenMiddleware.ts');

// Routes
app.use(cookies());
app.use(express.static(process.cwd() + "/public/"));

app.get('/', (req, res) => {
  res.sendFile(process.cwd() + '/public/index.html');
});

app.get('/auth', (req, res) => {
  res.clearCookie('token')
  res.sendFile(process.cwd() + '/public/login.html')
})

app.use(checkTokenMiddleware, express.static(process.cwd() + "/protected/"));

app.get('/admin', checkTokenMiddleware, (req, res) => {
  // Get the token from the cookies
  const token = req.cookies.token
  const decoded = jwt.decode(token, { complete: false })
  if (decoded.type === 'admin' && decoded.exp > Date.now() / 1000) {
    // If the user is admin and the token is valid
    res.sendFile(process.cwd() + '/protected/admin.html')
  }
})

io.on('connection', (socket) => {
  console.log('a user connected');
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});

/*
* Login system
* @description The login system for the application
*/
// Get the list of users from cred.json
var users = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/protected/cred.json')), "utf8");

// Auth on socket
io.on('connection', (socket) => {

  // Handle login
  socket.on('login', (data) => {
    console.log(data);
    // Checking
    const user = users.find(u => u.username === data.username)

    // If the user doesn't exist
    if (!user) {
      socket.emit('login', { message: 'bad_credentials' })
    } else {
      // If the password is wrong
      checkPassword(data.password, user.password).then((result) => {
        if (result) {
          const token = jwt.sign({
            id: user.id,
            type: user.type,
            username: user.username
          }, SECRET, { expiresIn: '2 hours' })

          socket.emit('login', { access_token: token })
        } else {
          socket.emit('login', { message: 'bad_credentials' })
        }
      })
    }
  })

  // Send the database to the client
  socket.on('getDatabase', () => {
    console.log("Database requested");

    // Get the current database
    let database = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/database.json'), "utf8"));
    const rooms = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/rooms.json'), "utf8"));
    const teachers = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/teachers.json'), "utf8"));
    const messages = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/messages.json'), "utf8"));

    // Get the current temp database
    const tempDatabase = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/database-temp.json'), "utf8"));

    // Compare the last modified date of the database and the temp database
    let temp = false;
    if (database.lastModified < tempDatabase.lastModified) {
      temp = true;
    }
    

    socket.emit('getDatabase', [database, rooms, teachers, messages, temp]);
  })

  // Send the temp database to the client
  socket.on('getTempDatabase', () => {
    console.log("Temp database requested");

    // Get the current temp database
    const tempDatabase = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/database-temp.json'), "utf8"));

    socket.emit('getTempDatabase', tempDatabase);
  })

  // Send the history list to the client
  socket.on('getHistory', () => {
    console.log("History requested");

    // Get the list of history files
    const files = fs.readdirSync(path.join(process.cwd(), '/data/history'));

    // Create an array of objects with the name and the date of the files
    let history = [];
    files.forEach(file => {
      history.push({
        name: file,
        date: fs.statSync(path.join(process.cwd(), '/data/history/' + file)).mtime
      })
    })

    socket.emit('getHistory', history);
  })

  // Load a specific history file
  socket.on('loadHistory', (file) => {
    console.log("History file requested:", file);

    try {
      // Load the file
      const data = JSON.parse(fs.readFileSync(path.join(process.cwd(), '/data/history/' + file), "utf8"));
      socket.emit('loadHistory', data);
    } catch (error) {
      console.error("Error loading the history file:", error);
      socket.emit('error', { message: 'Une erreur est survenue pendant le chargement de l\'historique.' });
    }
  })

  // Handle database update from admin panel
  socket.on('updateDatabase', (temp, data) => {
    console.log("Admin updated the database");
    try {
      if (temp == 1) {
        // Save the database in a temp file
        fs.writeFileSync(path.join(process.cwd(), '/data/database-temp.json'), JSON.stringify(data), "utf8");
        console.log("Database saved in a temp file");
      } else {
        // Save the previous database in the history folder (not exceeding 25 files)
        const files = fs.readdirSync(path.join(process.cwd(), '/data/history'));
        if (files.length > 25) {
          fs.unlinkSync(path.join(process.cwd(), '/data/history/' + files[0]));
        }
        fs.writeFileSync(path.join(process.cwd(), '/data/history/' + Date.now() + '.json'), JSON.stringify(data), "utf8");
        console.log("Previous database saved in the history folder");
        fs.writeFileSync(path.join(process.cwd(), '/data/database.json'), JSON.stringify(data), "utf8");
        console.log("Database saved");

        // Refresh the pages
        io.emit('refresh');
      }
    } catch (error) {
      console.error("Error updating the database:", error);
      socket.emit('error', { message: 'Une erreur est survenue pendant l\'enregistrement de la base de données.' });
    }

  })

})

// app.post('/login', (req, res) => {
//   console.log(req.body);
//   if (req.body.username == "" || req.body.password == "") {
//     return res.redirect('/auth?message=bad_credentials')
//   }
//   // Checking
//   const user = users.find(u => u.username === req.body.username)

//   // If the user doesn't exist
//   if (!user) {
//     return res.redirect('/auth?message=bad_credentials')
//   } else {
//     // If the password is wrong
//     checkPassword(req.body.password, user.password).then((result) => {
//       if (result) {
//         const token = jwt.sign({
//           id: user.id,
//           type: user.type,
//           username: user.username
//         }, SECRET, { expiresIn: '2 hours' })

//         return res.json({ access_token: token })
//       } else {
//         return res.redirect('/auth?message=bad_credentials')
//       }
//     })
//   }
// })

// Check if the password is correct
async function checkPassword(password, hash) {
  isSame = await bcrypt.compare(password, hash)
  console.log("checkPass : " + isSame)
  return isSame
}

