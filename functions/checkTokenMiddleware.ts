
const jwt = require('jsonwebtoken')
var cookies = require("cookie-parser");

// Every 24 hours, generate a new secret for the JWT
var SECRET = require('crypto').randomBytes(64).toString('hex')
setInterval(() => {
  SECRET = require('crypto').randomBytes(64).toString('hex')
}, 1000 * 60 * 60 * 24)

// Check token middleware
function checkTokenMiddleware(req, res, next) {
  // Get the token from the cookies
  const token = req.cookies.token

  // If the token doesn't exist
  if (!token || token === undefined) {
    // Remove the token from cookies
    res.clearCookie('token')
    return res.redirect('/auth?message=need_token')
  }

  // Check if the token is valid
  jwt.verify(token, SECRET, (err, decodedToken) => {
    if (err) {
      res.redirect('/auth?message=bad_token')
    } else {
      return next()
    }
  })
}

module.exports = { checkTokenMiddleware, SECRET }